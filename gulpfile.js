var gulp = require('gulp');
var browserSync = require('browser-sync');
var reload = browserSync.reload;

// watch files for changes and reload
gulp.task('serve', function() {
    browserSync({
        server: {
            baseDir: './src/',
            index: 'index.html',
            routes: {
                "/bower_components": "bower_components",
                "/src": "src"
            }
        }
    });

    gulp.watch([
        'src/**/*.js',
        'src/**/*.css',
        'src/**/*.html'
    ], {cwd: './'}, reload);
});
