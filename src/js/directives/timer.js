(function () {
    "use strict";
    angular.module('app').directive('timer', function () {
        return {
            restrict: 'E',
            scope: {
                data: '='
            },
            template: '{{item.timer}}',
            controller: ['$interval', '$scope', 'dateHelper', function ($interval, $scope, dateHelper) {
                var todoItem = this;

                todoItem.timer = '';

                function updateTime() {
                    var diff = Math.floor((todoItem.data.dateObj.getTime() - new Date().getTime()) / 1000);
                    if (diff <= 0) {
                        $interval.cancel(interval);
                        todoItem.timer = 'time is up';
                        return false;
                    }
                    todoItem.timer = dateHelper.dateFromTimestamp(diff);
                }

                var interval = $interval(updateTime, 1000);
                updateTime();

                $scope.$on('$destroy', function () {
                    $interval.cancel(interval);
                });
            }],
            controllerAs: 'item',
            bindToController: true
        };
    });
}());