/**
 * Created by Tek on 15.10.2015.
 */
(function () {
    "use strict";
    angular.module('app').controller('mainController', ['$filter', function ($filter) {
        function getFormData() {
            var date = new Date((new Date()).getTime() + 30 * 60000);

            return {
                tile: '',
                description: '',
                date: $filter('date')(date, 'dd.MM.yyyy'),
                time: $filter('date')(date, 'hh:mm'),
                dateObj: date
            }
        }

        this.tasks = [];
        this.form = null;

        this.removeTask = function (index) {
            this.tasks.splice(index, 1);
        };

        this.addTask = function () {
            //Constructing date from string
            var date = this.form.date.split('.').map(function (o) {
                return parseInt(o)
            });
            var time = this.form.time.split(':').map(function (o) {
                return parseInt(o)
            });
            this.form.dateObj = new Date(date[2], --date[1], date[0], time[0], time[1]);

            this.tasks.push(angular.copy(this.form));
            this.form = getFormData();
        };

        this.init = function () {
            this.form = getFormData();
        };
        this.init();
    }]);
}());